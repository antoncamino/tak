function Controls() {
  this.newgame = (function(){
    document.getElementById('newgame').onclick = function(){      
      this.disabled = "true";
      document.getElementById('turn').disabled = "true";
      gameset.games.push(new Game());
      gameset.lastGame().move(); 
      cellsInable(" ");
      clearHistory();    
    }
  }());

  this.clearhistory = (function(){
    document.getElementById('clearhistory').onclick = function(){
      clearHistory();
    }
  }());


  this.init = (function(){
    document.getElementById('init').onclick = function(){      
      document.getElementById('initial').setAttribute("style", "display:none"); 
      var pl1 = document.getElementById('pl1').value;
      var pl2 = document.getElementById('pl2').value;
      if(!pl1){
        pl1 = "Player1";
      }
      if(!pl2){
        pl2 = "Player2";
      }
      document.getElementById('p1').innerHTML = pl1;
      document.getElementById('p2').innerHTML = pl2;     
      document.getElementById('grid').setAttribute("style", "display:block");      
      document.getElementById('clearhistory').style.display = "block";
    }
  }());

  this.changeTurn = (function(){
    var t = document.getElementById('turn');
    t.onclick = function(){
      t.dataset.choise == 'x' ? t.dataset.choise = 'o' : t.dataset.choise = 'x';
      t.innerHTML = "swap turn to " + t.dataset.choise;
    }    
  }());
};

//helper functions
//
function cellsInable(val){
  var cells = document.getElementsByClassName('cell');
  for (var i = 0; i < 9; ++i){
    var cell = cells[i];
    cell.dataset.val = val;
    cell.innerHTML = " ";    
  }  
}

/*function AddButton(game){
  var p = createElement('p')
  p.id = 
  document.getElementById('historybtn').appendChild()
}*/

function disableCell(){
  var cells = document.getElementsByClassName('cell');
  for (var i = 0; i < 9; ++i){
    var cell = cells[i];
    cell.dataset.val = 'disable';       
  }  
}

function clearHistory(){
  for(var i = 0; i < 9; ++i){
    document.getElementById('target' + i).innerHTML = "";
  }
}

//gemset for saving sets
function GameSet(){};
GameSet.prototype = {
  games: [],
  score_pl1: 0,
  score_pl2: 0,
  lastGame: function(){    
    return this.games[this.games.length-1];
  }
};


//game 
function Game(){
  this.turn = document.getElementById('turn').dataset.choise;
  this.moves = [];
  this.count = 0;  
};

Game.prototype = new GameSet();




Game.prototype.check = function(count, obj){
  if(count < 9 && obj.dataset.val == ' '){
    return true;
  }else{
    return false;
  } 
};

Game.prototype.changeTurn = function(){
  this.turn == 'x' ? this.turn = 'o' : this.turn = 'x';
}

Game.prototype.move = function(){
  var self = this;
  var cells = document.getElementsByClassName('cell');  
  for (var i = 0; i < 9; ++i){
    var cell = cells[i];    
    cell.onclick = function(){      
      if(self.check(self.count, this)){
        this.innerHTML = self.turn;
        self.moves[self.count] = [" .", " .", " .", " .", " .", " .", " .", " .", " ."];
        self.getValues(self.moves[self.count]); 
        self.changeTurn();
        ++self.count;               
        this.dataset.val ="disabled";
        var result =  self.res();
        if(result){          
          a(self.moves);
          document.getElementById('newgame').disabled = false;
          disableCell();
          gameset.lastGame().setScore(result);
        } 
           
      }
    }
  }
}

Game.prototype.setScore = function(res){
  var p1 = document.getElementById('p1');
  var p2 = document.getElementById('p2');  
  if(p1.dataset.symb == res){    
    ++gameset.score_pl1;    
    document.getElementById('p1_s').innerHTML = gameset.score_pl1;
  }else{    
    ++gameset.score_pl2;    
    document.getElementById('p2_s').innerHTML = gameset.score_pl2;
  }
}



Game.prototype.getValues = function(arr){
  var cells = document.getElementsByClassName('cell');
  for (var i = 0; i < 9; ++i){
    var content = cells[i].innerHTML;
    if(!content){
      content = ".";
    }
    arr[i] = content;
  }
}

Game.prototype.clear = function(){
  for(var i = 0; i <= 6; i += 3) {
    if(anchors[i].innerHTML !== " " && anchors[i].innerHTML === anchors[i + 1].innerHTML && anchors[i + 1].innerHTML == anchors[i + 2].innerHTML) {            
      anchors[i].innerHTML = " ";      
    }
  }
}


Game.prototype.res = function(){
  var anchors = document.getElementsByClassName('cell');
  for(var i = 0; i <= 6; i += 3) {
    if(anchors[i].innerHTML !== " " && anchors[i].innerHTML === anchors[i + 1].innerHTML && anchors[i + 1].innerHTML == anchors[i + 2].innerHTML) {            
      return anchors[i].innerHTML;
    }
  }
  for(var i = 0; i <= 2 ; i++) {
    if(anchors[i].innerHTML !== " " &&  anchors[i].innerHTML === anchors[i + 3].innerHTML && anchors[i +3].innerHTML === anchors[i + 6].innerHTML) {            
      return anchors[i].innerHTML;
    }
  }
  for(var i = 0, j = 4; i <= 2 ; i = i + 2, j = j - 2) {
    if(anchors[i].innerHTML !== " " && anchors[i].innerHTML == anchors[i + j].innerHTML && anchors[i + j].innerHTML === anchors[i + 2*j].innerHTML) {           
      return anchors[i].innerHTML;
    }
  }

}


//init
var c = new Controls;
var gameset = new GameSet();
var a = function renderMove(arr) {
  var template = document.getElementById('template').innerHTML;  
  Mustache.parse(template);   // optional, speeds up future grids
  for(var i = 0; i < arr.length; ++i){    
    var rendered = Mustache.render(template, {arr1: arr[i][0], arr2: arr[i][1], arr3: arr[i][2], arr4: arr[i][3], arr5: arr[i][4], arr6: arr[i][5], arr7: arr[i][6], arr8: arr[i][7], arr9: arr[i][8]});
    document.getElementById('target' + i).innerHTML = rendered;
  }  
}